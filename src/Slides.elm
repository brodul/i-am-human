module Slides exposing (Message, Model, slides, subscriptions, update, view)

import Browser.Events exposing (onAnimationFrameDelta)
import Html exposing (Html, a, div, h1, img, li, p, small, text, ul)
import Html.Attributes exposing (href, src, style)
import Markdown
import SliceShow.Content exposing (..)
import SliceShow.Slide exposing (..)


{-| Model type of the custom content
-}
type alias Model =
    Float


{-| Message type for the custom content
-}
type alias Message =
    Float


{-| Type for custom content
-}
type alias CustomContent =
    Content Model Message


{-| Type for custom slide
-}
type alias CustomSlide =
    Slide Model Message


{-| Update function for the custom content
-}
update : Message -> Model -> ( Model, Cmd Message )
update elapsed time =
    ( time + elapsed, Cmd.none )


{-| View function for the custom content that shows elapsed time for the slide
-}
view : Model -> Html Message
view time =
    small
        [ style "position" "absolute", style "bottom" "0", style "right" "0" ]
        [ text
            ("the slide is visible for "
                ++ (round time // 1000 |> String.fromInt)
                ++ " seconds"
            )
        ]


{-| Inputs for the custom content
-}
subscriptions : Model -> Sub Message
subscriptions _ =
    onAnimationFrameDelta identity


{-| The list of slides
-}
slides : List CustomSlide
slides =
    [ [ item (h1 [] [ text "I am human" ])
      , item (p [] [ text "-- coding for humans" ])
      ]
    , [ fullImage "0" "assets/imgs/Sextant.jpg" ]
    , [ fullImage "0" "assets/imgs/Using_sextant_swing.gif" ]
    , [ item (h1 [] [ text "Lets build an application" ])
      , item (img [ src "assets/imgs/cal.png", style "max-width" "800px" ] [])
      ]
    , [ item (h1 [ style "font-size" "1.5em" ] [ text "Lets build a web application" ])
      , item (img [ src "assets/imgs/calweb.png", style "max-height" "400px" ] [])
      ]
    , [ item (h1 [] [ text "How not to do it for humans" ]) ]
    , [ item (h1 [] [ text "JavaScript ecosystem ..." ])
      , bullets
            [ bullet "JQuery" |> hide
            , bullet "Require JS" |> hide
            , bullet "Angular (JS)" |> hide
            , bullet "Backbone JS" |> hide
            , bullet "Ember JS" |> hide
            , bullet "React JS" |> hide
            , bullet "Vue JS" |> hide
            ]
      ]
    , [ item (h1 [] [ text "Ecosystem changes very fast" ])
      , bullets
            [ bullet "New features" |> hide
            , bullet "Maintanance Costs" |> hide
            ]
      ]
    , [ item (h1 [] [ text "There are 100 ways to do it" ])
      , item (p [] [ text "\"What can you make with 800,000 building blocks?\" -- npmjs.com" ]) |> hide
      , item (p [] [ text "\"What can you make with 10 000 kickens? Eggs?\" -- Evan Czaplicki (elmlang creator)" ]) |> hide
      ]
    , [ item (h1 [] [ text "Lets build a JS app today:" ])
      , bullets
            [ bullet "Package management! NPM, of course! Or Yarn!" |> hide
            , bullet "View layer/virtual DOM: React! Ember! Vue!" |> hide
            , bullet "Data flow and state management: Redux! Vuex! MobX?" |> hide
            , bullet "Immutable data structures! Immutable.js! Seamless! Mori!" |> hide
            , bullet "Strong and custom types: Flow! TypeScript!" |> hide
            , bullet "Dev server/hot reloading: Webpack! Parcel!" |> hide
            , bullet "Linters + transpilation: AirBnB styleguide, babel " |> hide
            , bullet "Auto-formating: Prettier" |> hide
            ]
      ]
    , [ item (h1 [] [ text "Runtime errors" ])
      , item (img [ src "assets/imgs/error.png", style "max-width" "800px" ] [])
      ]
    , [ fullImage "0" "assets/imgs/chan.jpg" ]
    , [ item (h1 [] [ text "I am human" ])
      , bullets
            [ bullet "Beginner friendly" |> hide
            , bullet "Simple to setup" |> hide
            , bullet "No runtime errors?" |> hide
            , bullet "Smart helpful compiler (I am a human, not a compiler)" |> hide
            , bullet "Something helpful ... that is hard to imagine" |> hide
            ]
      , item (img [ src "assets/imgs/unicorn.jpg", style "max-height" "250px" ] []) |> hide
      ]
    , [ item (h1 [ style "font-size" "1.5em" ] [ text "Beginner friendly (elm lang)" ])
      , item (img [ src "assets/imgs/elm.png", style "max-width" "400px" ] [])
      ]
    , [ item (h1 [ style "font-size" "1.5em" ] [ text "Simple to setup" ])
      , bullets
            [ bullet "Package management! Core (elm install)" |> hide
            , bullet "View layer/virtual DOM: Core" |> hide
            , bullet "Data flow and state management: Core (Elm arhitecture)" |> hide
            , bullet "Immutable data structures: Core" |> hide
            , bullet "Strong and custom types: Core" |> hide
            , bullet "Dev server/hot reloading: Core (elm reactor)" |> hide
            , bullet "Linters + transpilation: Core" |> hide
            , bullet "Auto-formating: elm-format" |> hide
            ]
      ]
    , [ code "elm" """foo num =
  if num > 16 then "demo code best code"
      """
      ]
    , [ item (img [ src "assets/imgs/no_else.png", style "max-width" "800px" ] [])
      ]
    , [ code "elm" """
foo num =
    if num > 16 then
        "demo code best code"

    else
        null
      """
      ]
    , [ item (h1 [] [ text "No null  ¯\\_(ツ)_/¯" ])
      , item (img [ src "assets/imgs/no_null.png", style "max-width" "800px" ] [])
      ]
    , [ code "elm" """
foo num =
    if num > 16 then
        "demo code best code"

    else
        "works"
      """
      ]
    , [ item (h1 [] [ text "Union types" ])
      , code "elm" """
type Bool = True | False
      """
      ]
    , [ item (h1 [] [ text "Maybe?" ])
      , code "elm" """
type Maybe a = Just a | Nothing
      """
      ]
    , [ code "elm" """
type Maybe a = Just a | Nothing

foo num =
    if num > 16 then
        Just "demo code best code"

    else
        Nothing
      """
      ]
    , [ code "elm" """
type Maybe a = Just a | Nothing

foo : Int -> Maybe String
foo num =
    if num > 16 then
        Just "demo code best code"

    else
        Nothing
      """
      ]
    , [ code "elm" """
bar num =
    case foo num of
        Just str ->
          String.toUpper str

      """
      ]
    , [ item (img [ src "assets/imgs/no_nothing.png", style "max-width" "800px" ] [])
      ]
    , [ code "elm" """
bar num =
    case foo num of
        Just str ->
          String.toUpper str

        Nothing -> "Ni ni"
      """ ]
    , [ item (h1 [] [ text "Resources:" ])
      , bullets
            [ bullet "React.js Conf 2016 - Jamison Dance - Rethinking All Practices"
            , bullet "ElmEurope 2018 - What is Success? - Evan Czaplicki"
            , bullet "Decoupled Days - Build Elegant UIs the Functional Way - Christopher Bloom"
            ]
      ]
    , [ item (h1 [] [ text "@brodul" ]) ]
    ]
        |> List.map paddedSlide


fullImage : String -> String -> CustomContent
fullImage top_str image_path =
    item
        (img
            [ src image_path
            , style "display" "block"
            , style "margin-left" "auto"
            , style "margin-right" "auto"
            , style "max-height" "500px"
            , style "position" "relative"
            , style "top" top_str
            ]
            []
        )


bullets : List CustomContent -> CustomContent
bullets =
    container (ul [])


bullet : String -> CustomContent
bullet str =
    item (li [] [ text str ])


bulletLink : String -> String -> CustomContent
bulletLink str url =
    item (li [] [ a [ href url ] [ text str ] ])


{-| Syntax higlighted code block, needs highlight.js in index.html
-}
code : String -> String -> CustomContent
code lang str =
    item (Markdown.toHtml [] ("```" ++ lang ++ "\n" ++ str ++ "\n```"))


{-| Custom slide that sets the padding and appends the custom content
-}
paddedSlide : List CustomContent -> CustomSlide
paddedSlide content =
    slide
        [ container
            (div [ style "padding" "50px 100px" ])
            content
        ]
