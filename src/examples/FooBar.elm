module Main exposing (bar, foo)


foo : Int -> Maybe String
foo num =
    if num > 16 then
        Just "demo code best code"

    else
        Nothing


bar num =
    case foo num of
        Just str ->
            String.toUpper str

        Nothing ->
            "No user"
