module Main exposing (foo)


foo : Int -> Maybe String
foo num =
    if num > 16 then
        Just "demo code best code"

    else
        Nothing
